import hashlib
import stripe
import os
import json
from flask import *
from flask_sqlalchemy import SQLAlchemy
from utils import json_response, JSON_MIME_TYPE
from werkzeug.utils import secure_filename
from users import dataProduk, idCategory, regis, yourLog, add, imgpro, remv, changeItem, remvItem, disCat, proEdit, getPass, getNewPass, updateProf, is_valid, produkData, dataAddCart, dataCart, remCart

#cadangan untuk SQLAlchemy
mdb = 'mysql+pymysql://user:password@host/database'
UPLOAD_FOLDER = 'static/uploads'
ALLOWED_EXTENSIONS = set(['jpeg', 'jpg', 'png', 'gif'])

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = mdb
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key='rahasia'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
pub_key = 'xxxxx'
secret_key = 'xxxx'
stripe.api_key = secret_key

db = SQLAlchemy(app)

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/")
def root():
    dataProdakNya=dataProduk()
    dataKategoriId=idCategory()
    dataProdakNya.update(dataKategoriId)

    if 'email' not in session:
        tampil=json.dumps(dataProdakNya)
        return json_response(tampil)
    
    email = session['email']
    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]
    dataProdakNya.update(firstName)
    dataProdakNya.update(noOfItems)
    tampil=json.dumps(dataProdakNya)
    return json_response(tampil)


@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'POST':
        password = request.form['password']
        email = request.form['email']
        firstName = request.form['firstName']
        lastName = request.form['lastName']
        address1 = request.form['address1']
        address2 = request.form['address2']
        zipcode = request.form['address2']
        if not zipcode:
            zipcode = ''
        city = request.form['city']
        if not city:
            city = ''
        state = request.form['state']
        if not state:
            state = ''
        country = request.form['country']
        if not country:
            country = ''
        phone = request.form['phone']
        if not phone:
            phone = ''
        regisnya=regis(password,email,firstName,lastName,address1,address2,zipcode,city,state,country,phone)
        tampil=json.dumps(regisnya)
        return json_response(tampil)


@app.route("/login", methods = ['POST', 'GET'])
def login():
    data=request.form
    if request.method == 'POST':
        email = data['email']
        password = data['password']
        if not all([data.get('email'), data.get('password')]):
            error = json.dumps({'error': 'Missing field/s (email, password )'})
            return json_response(error, 400)
        else:
            cek = is_valid(email, password)
            session['email'] = email
            if cek == True:
                return redirect(url_for('root'))
            else:
                error = json.dumps({'error': 'Email and password is not match!!'})
                return json_response(error, 401)


@app.route("/logout")
def logout():
    session.pop('email', None)
    return redirect(url_for('root'))


@app.route("/add")
def admin():
    kategori = add()
    tampil=json.dumps(kategori)
    return json_response(tampil)


@app.route("/addItem", methods=["GET", "POST"])
def addItem():
    if request.method == "POST":
        name = request.form['name']
        price = float(request.form['price'])
        description = request.form['description']
        stock = int(request.form['stock'])
        categoryId = int(request.form['category'])
        image = request.files['image']
        if image and allowed_file(image.filename):
            filename = secure_filename(image.filename)
            image.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        imagename = filename
        print(imagename)
        if not all([name,price,description,imagename,stock,categoryId]):
            error = json.dumps({'error': 'Missing field/s (name,price,description,imagename,stock,categoryId)'})
            return json_response(error, 400)

        imgpronya=imgpro(name,price,description,imagename,stock,categoryId)
        tampil=json.dumps(imgpronya)
        return json_response(tampil)


@app.route('/uploaddata', methods=['GET'])
def images():
    return render_template('add.html')


@app.route("/remove")
def remove():
    rEmv=remv()
    tampil=json.dumps(rEmv)
    return json_response(tampil)


@app.route("/removeItem/<int:productId>", methods=['GET','DELETE'])
def removeItem(productId):
    productId = request.args.get('productId')
    rItem=remvItem(productId)
    tampil=json.dumps(rItem)
    return json_response(tampil)


@app.route("/displayCategory/<int:categoryId>",methods=['GET'])
def displayCategory(categoryId):
    disCatName=disCat(categoryId)
    if 'email' not in session:
        tampil=json.dumps(disCatName)
    else:
        email = session['email']
        keyLog=yourLog(email)
        loggedIn=keyLog[0]
        firstName=keyLog[1]
        noOfItems=keyLog[2]
        disCatName.update(firstName)
        disCatName.update(noOfItems)
        tampil=json.dumps(disCatName)
    return json_response(tampil)


@app.route("/account/profile")
def profileHome():
    if 'email' not in session:
        return redirect(url_for('root'))
    
    email =  session['email']
    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]
    status={'status':200, 'message': 'You are logged in now!'}
    
    status.update(firstName)
    status.update(noOfItems)
    tampil = json.dumps(status)
    return json_response(tampil)


@app.route("/account/profile/edit")
def editProfile():
    if 'email' not in session:
        return redirect(url_for('root'))

    email = session['email']
    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]
    profil_data=proEdit(email)
    tampil = json.dumps(profil_data)
    return json_response(tampil)


@app.route("/loginForm")
def loginForm():
    if 'email' in session:
        return redirect(url_for('root'))
    status = {"your_status":"You need to loggin!"}
    tampil = json.dumps(status)
    return json_response(tampil)


@app.route("/account/profile/changePassword", methods=["GET", "POST"])
def changePassword():
    if 'email' not in session:
        return redirect(url_for('loginForm'))

    if request.method == "POST":
        oldPassword = request.form['oldpassword']
        oldPassword = hashlib.md5(oldPassword.encode()).hexdigest()
        newPassword = request.form['newpassword']
        newPassword = hashlib.md5(newPassword.encode()).hexdigest()

        email=session['email']
        getPassnya=getPass(email)

        userIdNya=getPassnya[0]
        yourPass=getPassnya[1]
        if yourPass == oldPassword:
            msg=getNewPass(newPassword,userIdNya)
        else:
            msg = {"password_change":"Wrong password!"}
        tampil = json.dumps(msg)
        return json_response(tampil)


@app.route("/updateProfile", methods=["GET", "POST"])
def updateProfile():
    if 'email' not in session:
        return redirect(url_for('loginForm'))
    #data = request.json
    if request.method == 'POST':
        email = data['email']
        firstName = request.form['firstName']
        lastName = request.form['lastName']
        address1 = request.form['address1']
        address2 = request.form['address2']
        if not zipcode:
            zipcode = ''
        city = request.form['city']
        if not city:
            city = ''
        state = request.form['state']
        if not state:
            state = ''
        country = request.form['country']
        if not country:
            country = ''
        phone = request.form['phone']
        if not phone:
            phone = ''
        updateProfile=updateProf(email, firstName, lastName, address1, address2,zipcode,city, state,country,phone)
        return updateProf


@app.route("/buy/<productId>", methods=['GET','POST'])
def buy(productId):
    dataProdukNya = produkData(productId)
    costumer = stripe.Costumer.create(email=request.form['stripeEmail'],source=request.form['stripeToken'])
    charge = stripe.Charge.create(
        costumer=costumer.id,
        amount='',
        currency='',
        description=''
    )
    channged=changeItem(email,productId)
    tampil = json.dumps(msg)
    return json_response(tampil)

@app.route("/payments/<int:productId>", methods=['GET','POST'])
def pay(productId):
    dataProdukNya = produkData(productId)
    if 'email' not in session:
        return redirect(url_for('loginForm'))

    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]
    pub_key={'pub_key':pub_key}
    dataProdukNya.update(firstName)
    dataProdukNya.update(noOfItems)
    dataProdukNya.update(pub_key)
    data=json.dumps(dataProdukNya)
    return json_response(data)
    #payment 


@app.route("/productDescription/<int:productId>", methods=['GET','POST'])
def productDescription(productId):
    dataProdukNya = produkData(productId)

    if 'email' not in session:
        data = json.dumps(dataProdukNya)
    email = session['email']
    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]
    dataProdukNya.update(firstName)
    dataProdukNya.update(noOfItems)
    data=json.dumps(dataProdukNya)
    return json_response(data)


@app.route("/addToCart/<int:productId>")
def addToCart(productId):
    if 'email' not in session:
        return redirect(url_for('loginForm'))

    email=session['email']
    addCart=dataAddCart(email, productId)
    return redirect(url_for('root'))


@app.route('/cart')
def cartnya():
    if 'email' not in session:
        return redirect(url_for('loginForm'))
    email = session['email']
    keyLog=yourLog(email)
    loggedIn=keyLog[0]
    firstName=keyLog[1]
    noOfItems=keyLog[2]

    email = session['email']
    cartnya = dataCart(email)
    data=json.dumps(cartnya)
    return json_response(data)


@app.route("/removeFromCart/<int:productId>")
def removeFromCart(productId):
    if 'email' not in session:
        return redirect(url_for('loginForm'))
    email = session['email']

    removeCart =  remCart(email,productId)
    return redirect(url_for('root'))


@app.errorhandler(404)
def page_note_found(e):
	return render_template('404-file.html')

if __name__ == '__main__':
    app.run(host='',debug=True)
