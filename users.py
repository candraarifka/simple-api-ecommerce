import pymysql
import json
import hashlib
import decimal
from utils import json_response, JSON_MIME_TYPE

conn = pymysql.connect("host", "user", "password", "database", charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
cursor = conn.cursor()

def dataProduk():
    sql = "SELECT productId, name, price, description, image, stock FROM products"
    cursor.execute(sql)
    allData1 = cursor.fetchall()
    results = [ dict(foo) for foo in allData1 ]
    result = {"status":200, "all_item":results}
    return result

def idCategory():
    sql = "SELECT categoryId, name FROM categories"
    cursor.execute(sql)
    allData2 = cursor.fetchall()
    results = [ dict(foo) for foo in allData2 ]
    result = {"category_data":results}
    return result

def regis(password,email,firstName,lastName,address1,address2,zipcode='',city='',state='',country='',phone=''):
    sql = """INSERT INTO users(password,email,firstName,lastName,address1,address2,zipcode,city,state,country,phone)
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    cursor.execute(sql,(hashlib.md5(password.encode()).hexdigest(),email,firstName,lastName,address1,address2,zipcode,city,state,country,phone))
    conn.commit()
    #conn.close()
    tampil = {
        "status": 200,
        "message": "You have been successfully registered!"
        }
    return tampil

def yourLog(email):
    loggedIn = True
    sql = "SELECT userId, firstName FROM users WHERE email=%s"
    cursor.execute(sql,(email))
    user = cursor.fetchone()
    sql = "SELECT count(productId) FROM cart WHERE userId=%s AND orders=0"
    cursor.execute(sql,(user['userId'],))
    noOfItems=cursor.fetchone()
    firstName = {'firstName':user['firstName']}
    jumlahItem = {'jumlahItem':noOfItems['count(productId)']}
    return (bool(loggedIn), firstName, jumlahItem)

def trylog(email,password):
    loggedIn = True
    print(loggedIn)
    sql = "SELECT userId, firstName FROM users WHERE email=%s"
    cursor.execute(sql,(email))
    user = cursor.fetchone()

    sql = "SELECT count(productId) FROM cart WHERE userId=%s AND orders=0"
    cursor.execute(sql,(user['userId'],))
    noOfItems=cursor.fetchone()
    msg = 'login sukses!'
    return msq

def add():
    sql = "SELECT categoryId, name FROM categories"
    cursor.execute(sql)
    categories = cursor.fetchall()
    results = [ dict(foo) for foo in categories ]
    result = {"status":200,"all_category":results}
    return result


def imgpro(name,price,description,image,stock,categoryId):
    try:
        cursor.execute('''INSERT INTO products (name, price, description, image, stock, categoryId) VALUES (%s, %s, %s, %s, %s, %s)''', (name, price, description, image, stock, categoryId))
        conn.commit()
        msg="added item successfully"
    except:
        conn.rollback()
        msg="error occured"
    conn.close()
    print(msg)
    result={"status":200, "item_upload":msg}
    return result

def remv():
    cursor.execute('SELECT productId, name, price, description, image, stock FROM products')
    data = cursor.fetchall()
    results = [ dict(foo) for foo in data ]
    result = {"status":200, "result_deleted":results}
    return result

def remvItem(productId):
    try:
        sql= "DELETE FROM products WHERE productID =%s"
        cursor.execute(sql,(productId,))
        conn.commit()
        msg = "Deleted successsfully"
    except:
        conn.rollback()
        msg = "Error occured"
    conn.close()
    result={"status":200, "image_deleted":msg}
    return result

def disCat(categoryId):
    sql="SELECT products.productId, products.name, products.price, products.image, categories.name FROM products, categories WHERE products.categoryId = categories.categoryId AND categories.categoryId =%s"
    cursor.execute(sql,(categoryId,))
    data=cursor.fetchall()
    results = [ dict(foo) for foo in data ]
    print(results)
    result = {"status":200, "semua_item":results}
    return result

def proEdit(email):
    sql="SELECT userId, email, firstName, lastName, address1, address2, zipcode, city, state, country, phone FROM users WHERE email =%s"
    cursor.execute(sql,(email,))
    profileData = cursor.fetchone()
    tampil = {"profile_data":profileData}
    return tampil

def getPass(email):
    sql="SELECT userId, password FROM users WHERE email =%s"
    cursor.execute(sql,(email,))
    user=cursor.fetchone()
    print(user)
    return (user['userId'], user['password'])

def getNewPass(newpassword,userId):
    try:
        cursor.execute("UPDATE users SET password = %s WHERE userId = %s", (newpassword, userId))
        conn.commit()
        msg="Changed password successfully"
    except:
        conn.rollback()
        msg = "Failed!!"
    conn.close()
    tampil = {"password_change":msg, "status":200}
    return tampil

def updateProf(email, firstName, lastName, address1, address2,zipcode,city, state,country,phone):
    try:
        sql="UPDATE users SET firstName =%s, lastName =%s, address1 =%s, address2 =%s, zipcode =%s, city =%s, state =%s, country =%s, phone =%s WHERE email =%s"
        cursor.execute(sql,(firstName, lastName, address1, address2, zipcode, city, state, country, phone, email))
        conn.commit()
        msg="Saved Successfull!"
    except:
        conn.rollback()
        msg="Error occured"
    conn.close()
    tampil = {"edit_profile":msg}
    return tampil

def produkData(productId):
    sql="SELECT productId, name, price, description, image, stock FROM products WHERE productId =%s"
    cursor.execute(sql,(productId,))
    prodData = cursor.fetchone()
    result = {"category_data":prodData}
    return result

def changeItem(email, productId):
    sql = "SELECT userId, firstName FROM users WHERE email=%s"
    cursor.execute(sql,(email))
    user = cursor.fetchone()
    userId = user['userId']

    sql = "UPDATE cart SET orders=1 WHERE productId=%s AND userId=%s"
    cursor.execute(sql,(productId,userId))

    msg = {"buy_item":"Successfull!"}
    return msg

def dataAddCart(email,productId):
    sql = "SELECT userId FROM users WHERE email =%s"
    cursor.execute(sql, (email,))
    user = cursor.fetchone()
    userId=user['userId']

    try:
        cursor.execute("INSERT INTO cart (userId, productId) VALUES (%s, %s)", (userId, productId))
        conn.commit()
        msg = "Added successfully"
    except:
        conn.rollback()
        msg = "Error occured"
    return msg

def dataCart(email):
    sql = "SELECT userId FROM users WHERE email=%s"
    cursor.execute(sql,(email,))
    user = cursor.fetchone()
    userId=user['userId']

    sql="SELECT products.productId, products.name, products.price, products.image FROM products, cart WHERE products.productId = cart.productId AND cart.userId =%s"
    cursor.execute(sql,(userId,))
    products = cursor.fetchall()
    results = [ dict(foo) for foo in products ]
    D = decimal.Decimal
    totalPrice=0
    for x in results:
        totalPrice += D(x['price'])

    productsNya = {"total_products":results}
    totalPriceNya = {"total_price":int(totalPrice)}
    productsNya.update(totalPriceNya)

    return productsNya

def remCart(email,productId):
    sql = "SELECT userId FROM users WHERE email=%s"
    cursor.execute(sql,(email,))
    userId = cursor.fetchone()[0]

    try:
        sql = "DELETE FROM cart WHERE userId=%s AND productId=%s"
        cursor.execute(sql, (userId,productId))
        conn.commit()
        msg = "removed successfully"
    except:
        conn.rollback()
        msg = "error occured"
        conn.close()
    return msg

def is_valid(email, password):
    sql="SELECT email, password FROM users WHERE email=%s AND password=%s"
    cursor.execute(sql,(email,hashlib.md5(password.encode()).hexdigest()))
    datas = cursor.fetchall()
    alldata = [data for data in datas if data['email'] == email and data['password'] == hashlib.md5(password.encode()).hexdigest()]
    if alldata:
        return True
    else:
        return False
