import pymysql

conn = pymysql.connect("host", "user", "password", "databases", charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
cursor = conn.cursor()

cursor.execute('''CREATE TABLE users
		(userId INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
		password varchar(255),
		email varchar(255),
		firstName varchar(255),
		lastName varchar(255),
		address1 varchar(255),
		address2 varchar(255),
		zipcode varchar(255),
		city varchar(255),
		state varchar(255),
		country varchar(255),
		phone varchar(255),
		timeLogin TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
		)''')

cursor.execute('''CREATE TABLE categories
		(categoryId INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
		name varchar(255)
		)''')

cursor.execute('''CREATE TABLE products
		(productId INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
		name varchar(255),
		price varchar(255),
		description varchar(255),
		image varchar(255),
		stock INTEGER,
		categoryId INTEGER,
		FOREIGN KEY(categoryId) REFERENCES categories(categoryId)
		)''')

cursor.execute('''CREATE TABLE cart
		(userId INTEGER PRIMARY KEY NOT NULL,
		productId INTEGER NOT NULL,
		orders BOOLEAN NOT NULL,
		FOREIGN KEY(userId) REFERENCES users(userId),
		FOREIGN KEY(productId) REFERENCES products(productId)
		)''')

conn.close()
